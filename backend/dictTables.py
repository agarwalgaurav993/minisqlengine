'''
Created on 31 Jan, 2017

@author: gaurav
'''
def getTableDict(fpath = "/home/gaurav/workspace/MiniSqlEngine/Data/metadata.txt"):
    f = open(fpath,'r')
    tables = {}
    flag = "stop"
    for line in f:
        if(line[:-1] == "<begin_table>"):
            flag = "start"
            atts = []
            tablename = ""
        elif(line[:-1] == "<end_table>"):
            if(tablename != ""):
                tables[tablename] = atts
        elif(flag == "start"):
            tablename = line[1:-2]
            flag = "stop"
        else:
            atts.append(line[1:-2])
    return tables