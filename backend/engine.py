'''
Created on 31 Jan, 2017

@author: gaurav
'''
from sqlparse.sql import IdentifierList, Identifier, Where, Function
from sqlparse.tokens import Keyword, DML, Wildcard
import re

def checkDistinct(stat):
    sflag = False
    for item in stat:
        if item.ttype is DML and str(item) == "SELECT":
            sflag = True
        elif sflag == True:
            if item.ttype is Keyword and str(item) == "DISTINCT":
                return True
    
    return False
            
            
def getIdentifiers(stat, toCheck, dstatus = False):
    sflag = False
    Key = Keyword
    if toCheck == "SELECT" and dstatus == False:
        Key = DML
    if(dstatus):
        toCheck = "DISTINCT"
    for item in stat:
        if item.ttype is Key and str(item) == toCheck:
            sflag = True
        elif sflag == True:
            if type(item) is Function or type(item) is IdentifierList or type(item) is Identifier or (item.ttype is Wildcard and (toCheck == "SELECT" or toCheck == 'DISTINCT')):
                return item


def getList(item):
    item = "".join(item.split())
    return item.split(',');
        
def getFields(parsedStr):
    dStatus = checkDistinct(parsedStr)
    attList = getList(str(getIdentifiers(parsedStr, "SELECT",  dStatus)))
    return attList

def getTables(parsedStr):
    tabList = getList(str(getIdentifiers(parsedStr, "FROM")))
    return tabList

def getConditions(stat):
    f = False
    for item in stat:
        if type(item) is Where and str(item).startswith("WHERE"):
            f = True
            break
    conditions = []
    if(not f):
        return conditions
    item = str(item).replace("WHERE", "")
    item = item.strip()
    join = ""
    if(item.find("OR")>-1):
        join = "OR"
    elif(item.find("AND")>-1):
        join = "AND"

    if(join != ""):
        conditions.append(join.split())
        item = item.replace(" "+join+" ", "~")
        for k in item.split('~'):
            conditions.append(k.split())
    else:
        conditions.append(item.split());
    return conditions

def checkFunctions(fields):
    try:
        fStatus = False
        c = 0
        l1, l2 = [], []
        for i in fields:
            matchObj  = re.match( r'([a-z]*)\(([a-zA-Z\.\*]*)\)', i, re.M|re.I)
            if matchObj:
                fStatus = True
                l1.append(matchObj.group(1))
                l2.append(matchObj.group(2))
            else:
                c = 1
        if fStatus and c == 1:
            raise Exception('Error: Aggregate Function and an attribute requested together')
        elif fStatus == False:
            return fields, []
        else:
            return l2, l1
    except Exception as err:
        print err.args[0]