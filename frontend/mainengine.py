'''
Created on 31 Jan, 2017

@author: gaurav
'''
from backend import dictTables, engine
import sqlparse
from backend.engine import getTables, getFields, getConditions, checkDistinct, checkFunctions
from __builtin__ import exit
import csv
import operator
import sys
tablespath = "/home/gaurav/workspace/MiniSqlEngine/Data/"
metadataPath = "/home/gaurav/workspace/MiniSqlEngine/Data/metadata.txt"
ops = { "+": operator.add, "=": operator.eq , "<": operator.lt, ">": operator.gt, ">=": operator.ge , "<=": operator.le} # etc.
def printres(dStat, res):
    if(dStat):
        output = []
        for x in res:
            if x not in output:
                output.append(x)
        res = output
    for r in res:
        print r

def check(arg):
    return ops[arg[1]](arg[0],arg[2])

def joinCheck(condList, proList, tblList):
    lhs = False
    rhs = False
    for condition in condList:
        if(len(condition) == 3):
            l = []
            if(not str(condition[0]).isdigit()):
                for j, tbl in enumerate(tblList):
                    if tbl == condition[0].split('.')[0]:
                        lhs = True
            if(not str(condition[2]).isdigit()):
                for j, tbl in enumerate(tblList):
                    if tbl == condition[2].split('.')[0]:
                        rhs = True
    if(lhs and rhs):
        i = 0
        pos = -1;
        while i<len(proList):
            j = i + 1;
            while j<len(proList):
                if proList[i].split('.')[2] == proList[j].split('.')[2]:
                    pos = j;
                    break;
                j = j + 1
            i = i + 1
        if(pos > -1):
            del proList[pos]
    
def conditionCheck(condList,ls,tblList, proList):
    if len(condList) == 0:
        return True;
    opList = []
    for condition in condList:
        if(len(condition) == 3):
            l = []
            if(not str(condition[0]).isdigit()):
                for j, tbl in enumerate(tblList):
                    if tbl == condition[0].split('.')[0]:
                        if ls[j][int(condition[0].split('.')[1])] == "" or ls[j][int(condition[0].split('.')[1])] == "NULL":
                            return False
                        l.append(int(ls[j][int(condition[0].split('.')[1])]))
            else:
                l.append(int(condition[0]))
            l.append(condition[1])
            if(not str(condition[2]).isdigit()):
                for j, tbl in enumerate(tblList):
                    if tbl == condition[2].split('.')[0]:
                        if ls[j][int(condition[2].split('.')[1])] == "" or ls[j][int(condition[2].split('.')[1])] == "NULL":
                            return False
                        l.append(int(ls[j][int(condition[2].split('.')[1])]))
            else:
                l.append(int(condition[2]))
            opList.append(l)
    if(len(condList) == 3):
        if(condList[0][0] == "AND"):
            return check(opList[0]) and check(opList[1])
        else:
            return check(opList[0]) or check(opList[1])
    else:
        return check(opList[0])
    

def recur(tblList, i, proList,res, condList, ls = []):
    if i == len(tblList):
        sol = []
        if(conditionCheck(condList, ls, tblList, proList)):
            for seq1, j in enumerate(proList):
                for seq2, k in enumerate(tblList):
                    if(j.split('.')[0] == k):
                        sol.append(ls[seq2][int(j.split('.')[1])])
                        break
            res.append("\t\t".join(sol))
    else:
        with open(tablespath + tblList[i] + '.csv','rb') as csvfile:
            line = csv.reader(csvfile)
            for row in line:
                ls.append(row)
                recur(tblList, i+1, proList,res, condList, ls)
                ls.pop()

def sumRows(index, recs):
    sum = 0
    count = 0;
    for r in recs:
        if r.split("\t\t")[index] != "NULL" and r.split("\t\t")[index] != "":
            count = count + 1
            sum = sum + int(r.split("\t\t")[index])
    return str(sum), str(count)

def avgRows(index, recs):
    sum, count = sumRows(index, recs)
    return str(int(sum)/int(count))
def  minRows(index, recs):
    mini = min([int(m.split("\t\t")[index]) for m in recs if m.split("\t\t")[index] != "NULL" or m.split("\t\t")[index] != ""])
    return str(mini)
def  maxRows(index, recs):
    maxi = max([int(m.split("\t\t")[index]) for m in recs if m.split("\t\t")[index] != "NULL" or m.split("\t\t")[index] != ""])
    return str(maxi)


try:
    if len(sys.argv) != 2:
        raise Exception('Error: Arguments to few or too many!!')
    else:
        sql = str(sys.argv[1])
    tables = dictTables.getTableDict(metadataPath)
    parsed = sqlparse.parse(sqlparse.format(sql, keyword_case='upper'))
    for parse in parsed:
        dStat = checkDistinct(parse)
        tblList = getTables(parse)
        proList = getFields(parse)
        cpproList = proList[:]
        #print "Project ",proList
        proList, funcs = checkFunctions(proList)
        #print proList, funcs 
        condList = getConditions(parse)
        #print condList
        for tbl in tblList:
            if tables.has_key(tbl) == False:
                raise Exception('Error: Table not present in database!!')
        hl = []
        nl = []
        for k, c in enumerate(proList):
            if c == "*":
                for tbl in tblList:
                    for j, i in enumerate(tables[tbl]):
                        hl.append(tbl+ "."+i)
                        nl.append(tbl+"." + str(j) + "."+i)
                if len(funcs) == 0:
                    cpproList = hl[:]
            else:
                f = False
                if(c.find('.') != -1):
                    t, a = c.split('.');
                    if tables.has_key(t) == False:
                        raise Exception("Error: Table " + t + " not in database")
                    if a in tables[t]:
                        f = True
                        hl.append(tbl+'.'+a)
                        nl.append(tbl+"."+ str(tables[tbl].index(a)) +"." + a)
                else:
                    for tbl in tblList:
                        if c in tables[tbl]:
                            hl.append(tbl+'.'+c)
                            nl.append(tbl+"."+ str(tables[tbl].index(c)) +"." + c)
                            f = True;
                            break
                if(not f):
                    raise Exception("Error: Attribute " + c + " not part of any of these table " + str(tblList))
        proList = nl        
        for condition in condList:
            for ix, item in enumerate(condition):
                f = True
                if item in ["=", ">", "<", "<=", ">="]:
                    f = True
                elif(not(item.isdigit() or item == "AND" or item == "OR")):
                    if(item.find('.') == -1):
                        f = False
                        for tbl in tblList:
                            if item in tables[tbl]:
                                condition[ix] = tbl+"."+ str(tables[tbl].index(item)) +"." + item;
                                f = True;
                                break
                    else:
                        t, a = item.split('.');
                        if tables.has_key(t) == False:
                            raise Exception("Error: Table " + t + " not in database")
                        if a in tables[t]:
                            condition[ix] = t + "." + str(tables[t].index(a)) + "." + a
                            f = True
                        else:
                            f = False
                if not f:
                    raise Exception("Error: " + item + " is not present in any of these table " + str(tblList))
        joinCheck(condList, proList, tblList)
        res = []
        head = []
        for it in proList:
            head.append(it.split('.')[0] + "." + it.split('.')[2])    
        
        recur(tblList, 0, proList, res, condList)
        if len(funcs) != 0:
            nl = []
            for i, f in enumerate(funcs):
                head[i] = f + "(" + head[i] + ")"
                if f == "sum":
                    nl.append(sumRows(i, res)[0])
                elif f == "avg":
                    nl.append(avgRows(i, res))
                elif f == "max":
                    nl.append(maxRows(i, res))
                elif f == "min":
                    nl.append(minRows(i, res))
                elif f == "count":
                    nl.append(sumRows(i, res)[1])
                else:
                    raise Exception('Error: Invalid Function ' + f)
            res = ["\t\t\t".join(nl)]
        printres(dStat, ['\t'.join(cpproList)] + res)
except Exception as err:
    print err.args[0]